<?php

namespace Scraper;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Scraper\Factories\InputFactory;
use Scraper\Factories\OutputFactory;

class App extends Command
{
    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * Configure
     */
    protected function configure()
    {
        $this
            ->setName('scraper')
            ->setDescription('Generate JSON from scraped page')
        ;
    }

    /**
     * Execute
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $source = "http://www.sainsburys.co.uk/webapp/wcs/stores/servlet/CategoryDisplay?msg=&langId=44&categoryId=185749&storeId=10151&krypto=21HjfvIWc6vHfEEGRnbESg%2Bfx1%2FcSXoJHrGQrHmgAyRBElznBnvvkJmT2H2N6O6J6xCoWGqrxOK9%0AfsFp6tgqDp7tJrXWwKKOwMSxjCugub8iuXpS2zde3NxtZ%2FGZjQMXSlcf7VTs%2F1qAB3Rv9knzxYne%0AdCHY1ReDikjgek9D6ReX2nnm40FBH5WlDAPw8b3DThhUNgghuSXunz2gcIkpOrjMM0sf2QmSgGHw%0A4iFbvBl49oLSKl1Ni3a0EOoSA2NzXSIhFok4LKsrFdG36mp88nPceK9Md1kvvSqGO6xQDVQ%3D";
        $type = "url";
        $output_type = "json";

        $input_data = InputFactory::factory($type, $source);
        $output = OutputFactory::factory($output_type, $input_data);

        echo $output;
    }

}

?>