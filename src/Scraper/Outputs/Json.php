<?php

namespace Scraper\Outputs;

use Scraper\Interfaces\OutputInterface;

class Json implements OutputInterface{

    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function returnOutput()
    {
        $json_data = [
            "results" => [],
            "total" => $this->data[1]
        ];

        foreach ($this->data[0] as $data) {
            $json_data["results"][] = $data;
        }

        return json_encode($json_data);
    }

}

?>