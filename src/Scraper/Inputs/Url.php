<?php

namespace Scraper\Inputs;

use Scraper\Interfaces\InputInterface;
use Goutte\Client;

class Url implements InputInterface
{
    private $output = [];

    private $client;

    private $total = 0;

    public function __construct($source)
    {
        $this->client = new Client();

        $data = $this->crawlUrl($source);
        $this->parse($data);
    }

    private function crawlUrl($url)
    {
        $data = $this->client->request('GET', $url);
        $status = $this->client->getResponse()->getStatus();

        if ($status === 200) {
            return $data;
        } else {
            die("Crawling is unsuccessful! Status code: "+$status);
        }
    }


    private function parse($source)
    {
        $products = $source->filter(".productInfo > h3 > a");

        foreach($products as $data) {
            $this->getData($data);
        }
    }

    private function getData($data)
    {
        // Follow URL
        $url = $data->getAttribute("href");
        $page_data = $this->crawlUrl($url);

        $data = [
            "title"         => $page_data->filter(".productSummary > h1")->text(),
            "unit_price"    => (float)str_replace(["£", "/unit"],"", trim($page_data->filter(".pricing > .pricePerUnit")->text())),
            "size"          => round(strlen($page_data->html()) / 1024, 1) . 'kb',
            "description"   => ""
        ];

        if ($page_data->filterXPath('//*[@id="information"]/productcontent/htmlcontent/div[1]')->count())
        {
            $data["description"] = trim($page_data->filterXPath('//*[@id="information"]/productcontent/htmlcontent/div[1]')->text());
        }

        $this->total += $data["unit_price"];
        $this->output[] = $data;

    }

    public function getTheParsedData()
    {
        return array($this->output, $this->total);
    }
}

?>