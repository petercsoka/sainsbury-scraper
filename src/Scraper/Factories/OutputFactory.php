<?php

namespace Scraper\Factories;

abstract class OutputFactory {

    static public function factory($type, $source){

        switch($type) {

            case "json": $input = new \Scraper\Outputs\Json($source); break;

            default: die("No output found for:" . $type . "\n");
        }

        return $input->returnOutput();

    }

}

?>