<?php

namespace Scraper\Factories;

abstract class InputFactory {

    static public function factory($type, $source){

        switch($type) {

            case "url": $input = new \Scraper\Inputs\Url($source); break;

            default: die("No input found for:" . $type . "\n");
        }

        return $input->getTheParsedData();

    }

}

?>