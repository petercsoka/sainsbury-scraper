<?php

namespace Scraper\Interfaces;

interface InputInterface {

    public function __construct($source);

    public function getTheParsedData();

}

?>