<?php

class UrlTest extends PHPUnit_Framework_TestCase {

    private $obj;

    public function setUp()
    {
        parent::setUp();
        $this->source = "http://www.sainsburys.co.uk/webapp/wcs/stores/servlet/CategoryDisplay?msg=&langId=44&categoryId=185749&storeId=10151&krypto=21HjfvIWc6vHfEEGRnbESg%2Bfx1%2FcSXoJHrGQrHmgAyRBElznBnvvkJmT2H2N6O6J6xCoWGqrxOK9%0AfsFp6tgqDp7tJrXWwKKOwMSxjCugub8iuXpS2zde3NxtZ%2FGZjQMXSlcf7VTs%2F1qAB3Rv9knzxYne%0AdCHY1ReDikjgek9D6ReX2nnm40FBH5WlDAPw8b3DThhUNgghuSXunz2gcIkpOrjMM0sf2QmSgGHw%0A4iFbvBl49oLSKl1Ni3a0EOoSA2NzXSIhFok4LKsrFdG36mp88nPceK9Md1kvvSqGO6xQDVQ%3D";
        $this->obj = new \Scraper\Inputs\Url($this->source);

    }

    public function testGetTheParsedData()
    {
        $data = $this->obj->getTheParsedData();

        $this->assertTrue(is_array($data));
        $this->assertSame(count($data),2);
        $this->assertGreaterThan(0, count($data[0]));
        $this->assertGreaterThan(0, $data[1]);

        $this->assertArrayHasKey("title",$data[0][0]);
        $this->assertArrayHasKey("unit_price",$data[0][0]);
        $this->assertArrayHasKey("size",$data[0][0]);
        $this->assertArrayHasKey("description",$data[0][0]);

        $this->assertNotEmpty($data[0][0]["title"]);
        $this->assertNotEmpty($data[0][0]["unit_price"]);
        $this->assertNotEmpty($data[0][0]["size"]);


    }

}

?>