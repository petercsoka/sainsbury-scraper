# Sainsbury's page scraper

### Set up
    ```composer update```

### Run the app
    Run the ```php app.php scraper``` command to exexute the script. It will parse the predefined URL.

### Test
    Run the ```phpunit``` command
