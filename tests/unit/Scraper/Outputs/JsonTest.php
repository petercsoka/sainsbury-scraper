<?php

class JsonTest extends PHPUnit_Framework_TestCase {

    private $data;

    private $obj;

    public function setUp()
    {
        $this->data = array(
            array(
                array(
                    "title"         => "title",
                    "unit_price"    => 3.4,
                    "size"          => '60kb',
                    "description"   => "No description"
                )
            ),
            85.4
        );

        $this->obj = new \Scraper\Outputs\Json($this->data);
    }

    public function testReturnOutput()
    {
        $return = $this->obj->returnOutput();

        //Is valid json?
        $this->assertTrue(is_string($return));
        $this->assertTrue(is_object(json_decode(($return))));
    }


}
