<?php

error_reporting(1);
ini_set('display_errors', 1);

require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\Console\Application;

use Scraper\App;

$application = new Application();
$application->add(new App);
$application->run();

?>