<?php

namespace Scraper\Interfaces;

interface OutputInterface {

    public function __construct(array $data);

    public function returnOutput();

}

?>